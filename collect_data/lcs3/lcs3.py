import requests
import json

url = "http://45.77.243.33:8080/api/auth/login"

params={"username":"view_demo@gmail.com", "password": "viewdemo"}
headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, json=params)

data = json.loads(response.text)

headers = {
    'Accept': 'application/json',
    'X-Authorization': 'Baerer {}'.format(data['token']),
}
# print(data['token'])
params = (
    ('limit', '100000'),
    ('agg', 'NONE'),
    ('useStrictDataTypes', 'false'),
    ('keys', 'PM1_0,PM2_5,celTemp,cntErrLTE,cntErrSD,cntErrWifi,PM10_0,RH'),
    ('startTs', '1614966300000'),
    ('endTs', '1620236700000'),
)

response2 = requests.get('http://45.77.243.33:8080/api/plugins/telemetry/DEVICE/43668360-50d5-11eb-8be6-5b3b27a1556c/values/timeseries', headers=headers, params=params)
data2 = json.loads(response2.text)
with open('data2.json', 'w') as outfile:
    json.dump(data2, outfile)