import json
from datetime import datetime
import pandas as pd

f = open('data2.json', )

data = json.load(f)

# print(len(data['PM1_0']))
time = []
pm25 = []
pm10 = []
pm100 = []
celTemp = []
cntErrLTE = []
cntErrSD = []
cntErrWifi = []
RH = []

for i in range(len(data['PM1_0'])):
    unixTime = int(data["PM1_0"][i]["ts"]) / 1000
    date = datetime.fromtimestamp(unixTime).strftime('%m-%d-%Y %H:%M:%S')
    time.append(date)

    pm25.append(data["PM2_5"][i]["value"])
    pm10.append(data["PM1_0"][i]["value"])
    pm100.append(data["PM10_0"][i]["value"])
    celTemp.append(data["celTemp"][i]["value"])
    cntErrLTE.append(data["cntErrLTE"][i]["value"])
    cntErrSD.append(data["cntErrSD"][i]["value"])
    cntErrWifi.append(data["cntErrWifi"][i]["value"])
    RH.append(data["RH"][i]["value"])

df = pd.DataFrame({'time': time,
                   'pm1.0': pm10,
                   'pm2.5': pm25,
                   'pm10.0': pm100,
                   'celTemp': celTemp,
                   'cntErrLTE': cntErrLTE,
                   'cntErrSD': cntErrSD,
                   'cntErrWifi': cntErrWifi,
                   'RH': RH})

df.to_csv('data2.csv',index=False)