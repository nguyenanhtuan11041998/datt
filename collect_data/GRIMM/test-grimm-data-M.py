import csv
import pandas as pd

import os
inp_dir = r"C:\Users\Tuan\Desktop\doan\data\GRIMM03 DATA-20210506T080024Z-001\GRIMM03 DATA"
for inp in os.listdir(inp_dir):
    if inp.endswith("-M.dat"):
        print(inp)
        data = pd.read_csv(os.path.join(inp_dir, inp), sep='\t', header=None, skipinitialspace=True)
        data.columns = ["date & time", "PM10", "PM2.5", "PM1"]
        out_dir = r"C:\Users\Tuan\Desktop\doan\data\GRIMM03 DATA-20210506T080024Z-001\GRIMM03-csv-M"
        data.to_csv(os.path.join(out_dir,inp.replace(".dat", ".csv")), index = False, encoding='utf-8-sig')
