import csv
import pandas as pd

import os
inp_dir = r"C:\Users\Tuan\Desktop\doan\data\GRIMM03 DATA-20210506T080024Z-001\GRIMM03 DATA"
for inp in os.listdir(inp_dir):
    if inp.endswith("C.dat"):
        print(inp)
        data = pd.read_csv(os.path.join(inp_dir, inp), sep='\t', header=None, skipinitialspace=True)
        data.columns = ["date & time", "0.265 µm", "0.290 µm", "0.325 µm", "0.375 µm", "0.425 µm", "0.475 µm", "0.540µm",
                        "0.615 µm", "0.675 µm", "0.750 µm", "0.900 µm", "1.150 µm", "1.450 µm", "1.800 µm","2.250 µm",
                        "2.750 µm", "3.250 µm", "3.750 µm", "4.500 µm", "5.750 µm", "7.000 µm", "8.000 µm", "9.250 µm",
                        "11.250 µm", "13.750 µm", "16.250 µm", "18.750 µm", "22.500 µm", "27.500 µm", "31.000 µm", "34.000 µm"]
        out_dir = r"C:\Users\Tuan\Desktop\doan\data\GRIMM03 DATA-20210506T080024Z-001\GRIMM03-csv"
        data.to_csv(os.path.join(out_dir,inp.replace("-C.dat", ".csv")), index = False, encoding='utf-8-sig')

# inp_dir = r"C:\Users\Tuan\Desktop\doan\data\GRIMM03 DATA-20210506T080024Z-001\GRIMM03 DATA\GRIMM03-DATA-C5_2021-05-01-C.dat"
# data = pd.read_csv(inp_dir, sep='\t', header=0, skipinitialspace=True)
# data.columns = ["date & time", "0.265 µm", "0.290 µm", "0.325 µm", "0.375 µm", "0.425 µm", "0.475 µm", "0.540 µm",
#                 "0.615 µm", "0.675 µm", "0.750 µm", "0.900 µm", "1.150 µm", "1.450 µm", "1.800 µm", "2.250 µm",
#                 "2.750 µm", "3.250 µm", "3.750 µm", "4.500 µm", "5.750 µm", "7.000 µm", "8.000 µm", "9.250 µm",
#                 "11.250 µm", "13.750 µm", "16.250 µm", "18.750 µm", "22.500 µm", "27.500 µm", "31.000 µm", "34.000 µm"]
# out_dir = r"C:\Users\Tuan\Desktop\doan\data\GRIMM03 DATA-20210506T080024Z-001\GRIMM03-csv-v2\GRIMM03-DATA-C5_2021-05-01-C.csv"
# data.to_csv(out_dir, index=False)

# inp_dir = r"C:\Users\Tuan\Desktop\doan\data\GRIMM03 DATA-20210506T080024Z-001\GRIMM03 DATA\GRIMM03-DATA-C5_2021-05-01-C.dat"
# data = pd.read_fwf(inp_dir)
# print(data)
# data.columns = ["date & time", "0.265 µm", "0.290 µm", "0.325 µm", "0.375 µm", "0.425 µm", "0.475 µm", "0.540 µm",
#                 "0.615 µm", "0.675 µm", "0.750 µm", "0.900 µm", "1.150 µm", "1.450 µm", "1.800 µm", "2.250 µm",
#                 "2.750 µm", "3.250 µm", "3.750 µm", "4.500 µm", "5.750 µm", "7.000 µm", "8.000 µm", "9.250 µm",
#                 "11.250 µm", "13.750 µm", "16.250 µm", "18.750 µm", "22.500 µm", "27.500 µm", "31.000 µm", "34.000 µm"]
# out_dir = r"C:\Users\Tuan\Desktop\doan\data\GRIMM03 DATA-20210506T080024Z-001\GRIMM03-csv\GRIMM03-DATA-C5_2021-05-01-C.csv"
# data.to_csv(out_dir, index=False)