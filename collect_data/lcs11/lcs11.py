import requests
import json
import csv
from datetime import datetime, timedelta

url = r'https://aqm-iot-service.mybluemix.net/api/devices/LL01_LL01/history?type=minutes&minutes=60000&index=PM25'

resp = requests.get(url)
data = json.loads(resp.text)

data = data['record_history']
print(data)

csv_columns = ["updated_time", "originalValue", "value"]
path = 'test.csv'
with open(path, 'a', newline='') as outfile:
    writer = csv.DictWriter(outfile, fieldnames=csv_columns)
    for i in data:
        # print(i)
        date = datetime.strptime(i['time'], '%Y-%m-%dT%H:%M:%S.%fZ') + timedelta(hours=7)
        print(date)
        # print(date)
        result = {
            "updated_time": date,
            "originalValue": i['data'][0]['originalValue'],
            "value": i['data'][0]['value']
        }

        if outfile.tell() == 0:
            writer.writeheader()
        # writer.writeheader()
        writer.writerow(result)
