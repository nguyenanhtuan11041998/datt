import requests
import json
import csv

url = "https://aqm-iot-service.mybluemix.net/api/devices/LL01_LL01/history?type=oneday&index=PM25&startDate=2020-12-22&"
x = requests.get(url)

y = json.loads(x.text)

# print (y["record_history"])
employee_data = (y["record_history"]["min"],["min_vn"])

data_file = open('data_file.csv', 'w') 
csv_writer = csv.writer(data_file)

count = 0
  
for emp in employee_data: 
    if count == 0: 
  
        # Writing headers of CSV file 
        header = emp.keys() 
        csv_writer.writerow(header) 
        count += 1
  
    # Writing data of CSV file 
    csv_writer.writerow(emp.values()) 
  
data_file.close() 