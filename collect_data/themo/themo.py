from pywinauto.application import Application
from pywinauto.keyboard import send_keys
import time
seconds = time.time()
import psutil


path = r"C:\Program Files (x86)\TR-7wb_nw for Windows\Tr7WF.exe"

connect = Application(backend='uia').start(path).connect(title='TR-7wb/nw for Windows', timeout=1000)
app = connect.TR7wbnwforWindows

download = app.child_window(title="Download", auto_id="1009", control_type="Button").wrapper_object()
download.click_input()

filename = app.child_window(title="File name:", auto_id="1001", control_type="Edit").wrapper_object()
filename.set_text(r'C:\data\tuan{}'.format(seconds))

save = app.child_window(title="Save", auto_id="1", control_type="Button").wrapper_object()
save.click_input()

time.sleep(2)

okButton = app.child_window(title="OK", auto_id="2", control_type="Button").wrapper_object()
okButton.click_input()



graphConnect = Application(backend='uia').connect(title='tuan{}.trz - T&D Graph'.format(seconds), timeout=1000)
graphApp = graphConnect["tuan{}trzTDGraph".format(seconds)]
# print(a.button(0))
# exit = a.button(0).children()
# print(exit)
# appClose = graphApp.Exit.wrapper_object()
# appClose.click_input()

# graphApp.print_control_identifiers()
a = graphApp.child_window(title="Drawing", auto_id="59392", control_type="ToolBar").wrapper_object()
# b = a.child_window(title="File", control_type="Button").wrapper_object()
# x = graphApp.child_window(title="Menu Bar").set_focus()
# print (x[0])
# :590 t:626 r:1710 b:630
# time.sleep(2)
a.button(3).click_input()
# pywinauto.mouse.click(coords=(165,110))
#
filename = graphApp.child_window(title="File name:", auto_id="1001", control_type="Edit").wrapper_object()
filename.set_text(r'C:\data\\tuan{}.csv'.format(seconds))
# #
save = graphApp.child_window(title="Save", auto_id="1", control_type="Button").wrapper_object()
save.click_input()
time.sleep(3)
# #
graphClose = graphApp.child_window(title="Close", control_type="Button").wrapper_object()
graphClose.click_input()

appClose = app.child_window(title="File", control_type="MenuItem").wrapper_object()
appClose.click_input()
appClose = app.child_window(title="Quit", auto_id="57665", control_type="MenuItem", found_index=1).wrapper_object()
appClose.click_input()

PROCNAME = "TandDGraph.exe"

for proc in psutil.process_iter():
    # check whether the process name matches
    if proc.name() == PROCNAME:
        proc.kill()
