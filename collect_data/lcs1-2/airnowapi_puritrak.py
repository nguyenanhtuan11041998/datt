import requests
from requests.structures import CaseInsensitiveDict
import json
import csv

url = "https://outdoor.airnow.vn:8443/v2/userdevice/get-outdoor-aqi-for-devices-public/"

headers = CaseInsensitiveDict()
headers["Content-Type"] = "application/x-www-form-urlencoded"

# data = "device_id_list=d000000000043&start_time=2020-12-23 00:00:00.000&end_time=2021-06-1 00:00:00.000&start_index=0&end_index=100000"
data = "device_id_list=d200010001001&start_time=2020-12-23 00:00:00.000&end_time=2021-06-1 00:00:00.000&start_index=0&end_index=100000"

resp = requests.post(url, headers=headers, data=data)

data = json.loads(resp.text)

csv_columns = ["updated_time","aqi","pm1","pm2_5","pm10","h","co2","t"]
# path = "d000000000043_2021-12_2021-5.csv"
path = "d200010001001_2021-12_2021-5.csv"
with open(path, 'a', newline='') as outfile:
    writer = csv.DictWriter(outfile, fieldnames=csv_columns)
    # for i in data["data"]["d000000000043"]:
    for i in data["data"]["d200010001001"]:
        result = {
            "updated_time": i["updated_time"],
            "aqi": i["aqi"],
            "pm1": i["pm1"],
            "pm2_5": i["pm2_5"],
            "pm10": i["pm10"],
            "h": i["h"],
            "co2": i["co2"],
            "t": i["t"]
        }
        if outfile.tell() == 0:
            writer.writeheader()
        # writer.writeheader()
        writer.writerow(result)
