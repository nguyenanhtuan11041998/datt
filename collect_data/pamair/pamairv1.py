import jwt
import requests
import json

payload = {"iat": 1610536526, 
    "exp": 1736766926,
    "aud":"/services/hourly",
    "sub": "d44bc3cc-7a97-4b3a-9497-f05f8743e921",
    "iss":"pamair-partner"}

key = "1a40b8f3-4d40-4835-acd0-46bc9933c605"
headers = {"typ": "JWT","alg": "HS256"}

encoded_jwt = jwt.encode(payload=payload, key='1a40b8f3-4d40-4835-acd0-46bc9933c605', algorithm="HS256", headers=headers)
# print(encoded_jwt)
options = {"verify_signature": True, "verify_aud": False, "exp": True}
x = jwt.decode(encoded_jwt, key, algorithms="HS256", options=options)
print (x)
reqHeaders = {
    "Content-Type":"application/json",
    "Clientid":"d44bc3cc-7a97-4b3a-9497-f05f8743e921",
    "Authorization": "Bearer {}".format(encoded_jwt)
    # "Authorization":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MTA1MzY1MjYsImV4cCI6MTczNjc2NjkyNiwiYXVkIjoiL3NlcnZpY2VzL2hvdXJseSIsInN1YiI6ImQ0NGJjM2NjLTdhOTctNGIzYS05NDk3LWYwNWY4NzQzZTkyMSIsImlzcyI6InBhbWFpci1wYXJ0bmVyIn0.j5COGJflblwWmKuxF2bAgbLAX9lyeguEPh_0iv0yLlc"
}

reqHeaders2 = {
    "Content-Type":"application/json",
    "Clientid":"d44bc3cc-7a97-4b3a-9497-f05f8743e921",
    # "Authorization": "Bearer {}".format(encoded_jwt)
    "Authorization":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MTA1MzY1MjYsImV4cCI6MTczNjc2NjkyNiwiYXVkIjoiL3NlcnZpY2VzL2hvdXJseSIsInN1YiI6ImQ0NGJjM2NjLTdhOTctNGIzYS05NDk3LWYwNWY4NzQzZTkyMSIsImlzcyI6InBhbWFpci1wYXJ0bmVyIn0.j5COGJflblwWmKuxF2bAgbLAX9lyeguEPh_0iv0yLlc"
}

# print (reqHeaders)

params = {"lat":"21.0054984","lon": "105.8422458"}

url = "https://api.pamair.org/services/hourly"
res = requests.post(url, headers=reqHeaders, json=params)
res2 = requests.post(url, headers=reqHeaders2, json=params) 
# print (res.text)
y = json.loads(res.text)
y2 = json.loads(res2.text)
with open ("../test.json", "w") as outfile:
    json.dump(y, outfile)
with open ("../test2.json", "w") as out:
    json.dump(y2, out)