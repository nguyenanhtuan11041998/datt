import jwt
import requests
import json
from datetime import datetime
import csv

payload = {"iat": 1610536526,
    "exp": 1736766926,
    "aud":"/services/hourly",
    "sub": "d44bc3cc-7a97-4b3a-9497-f05f8743e921",
    "iss":"pamair-partner"}

key = "1a40b8f3-4d40-4835-acd0-46bc9933c605"
headers = {"typ": "JWT","alg": "HS256"}

encoded_jwt = jwt.encode(payload=payload, key='1a40b8f3-4d40-4835-acd0-46bc9933c605', algorithm="HS256", headers=headers)

options = {"verify_signature": True, "verify_aud": False, "exp": True}
x = jwt.decode(encoded_jwt, key, algorithms="HS256", options=options)
reqHeaders = {
    "Content-Type":"application/json",
    "Clientid":"d44bc3cc-7a97-4b3a-9497-f05f8743e921",
    "Authorization": "Bearer {}".format(encoded_jwt)
}

params = {"lat":"21.0054334","lon": "105"}

url = "https://api.pamair.org/services/hourly"
res = requests.post(url, headers=reqHeaders, json=params)
data = json.loads(res.text)

unixTime = int(data["data"]["temp"]["longtime"])/1000
date = datetime.fromtimestamp(unixTime).strftime('%m-%d-%Y %H:%M:%S')
dayPath = date.split(" ")[0]

params_location2 = {"lat":"21.0054984","lon": "105"}
res_location2 = requests.post(url, headers=reqHeaders, json=params_location2)
data_location2 = json.loads(res_location2.text)

result ={
    "time": date,
    "temp_location1": data["data"]["temp"]["value"],
    "hum_location1": data["data"]["hum"]["value"],
    "pm25_location1": data["data"]["pm25"]["value"],
    "aqi_location1": data["data"]["aqi"]["value"],
    "temp_location2": data_location2["data"]["temp"]["value"],
    "hum_location2": data_location2["data"]["hum"]["value"],
    "pm25_location2": data_location2["data"]["pm25"]["value"],
    "aqi_location2": data_location2["data"]["aqi"]["value"]
}

csv_columns = ["time","temp_location1","hum_location1","pm25_location1","aqi_location1","temp_location2","hum_location2","pm25_location2","aqi_location2"]
path = "{}.csv".format(dayPath)
with open(path, 'a', newline='') as outfile:
    writer = csv.DictWriter(outfile, fieldnames=csv_columns)
    if outfile.tell() == 0:
        writer.writeheader()
    # writer.writeheader()
    writer.writerow(result)

